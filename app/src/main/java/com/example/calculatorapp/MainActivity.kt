package com.example.calculatorapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.example.calculatorapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val frameButton: Button = binding.frameButton
        val linearButton: Button = binding.linearButton
        val constraintButton: Button = binding.constraintButton

        frameButton.setOnClickListener {
            startActivity(Intent(this, FrameCalculatorActivity::class.java))
        }

        linearButton.setOnClickListener {
            startActivity(Intent(this, LinearCalculatorActivity::class.java))
        }

        constraintButton.setOnClickListener {
            startActivity(Intent(this, ConstraintCalculatorActivity::class.java))
        }
    }
}