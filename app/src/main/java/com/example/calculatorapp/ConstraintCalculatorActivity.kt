package com.example.calculatorapp

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.calculatorapp.databinding.ConstraintCalculatorLayoutBinding

class ConstraintCalculatorActivity : AppCompatActivity() {
    lateinit var binding: ConstraintCalculatorLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ConstraintCalculatorLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onResume() {
        val displayTextView: TextView = findViewById(R.id.display_text_view)
        super.onResume()
        val button1: Button = binding.buttonOne
        button1.setOnClickListener {
            val currentText = displayTextView.text.toString()
            val buttonText = binding.buttonOne.text.toString()
            displayTextView.text = currentText + buttonText
        }
        val button2: Button = binding.buttonTwo
        button2.setOnClickListener {
            val currentText = displayTextView.text.toString()
            val buttonText = binding.buttonTwo.text.toString()
            displayTextView.text = currentText + buttonText
        }

        val button3: Button = binding.buttonThree
        button3.setOnClickListener {
            val currentText = displayTextView.text.toString()
            val buttonText = binding.buttonThree.text.toString()
            displayTextView.text = currentText + buttonText
        }

        val button4: Button = binding.buttonFour
        button4.setOnClickListener {
            val currentText = displayTextView.text.toString()
            val buttonText = binding.buttonFour.text.toString()
            displayTextView.text = currentText + buttonText
        }

        val button5: Button = binding.buttonFive
        button5.setOnClickListener {
            val currentText = displayTextView.text.toString()
            val buttonText = binding.buttonFive.text.toString()
            displayTextView.text = currentText + buttonText
        }

        val button6: Button = binding.buttonSix
        button6.setOnClickListener {
            val currentText = displayTextView.text.toString()
            val buttonText = binding.buttonSix.text.toString()
            displayTextView.text = currentText + buttonText
        }

        val button7: Button = binding.buttonSeven
        button7.setOnClickListener {
            val currentText = displayTextView.text.toString()
            val buttonText = binding.buttonSeven.text.toString()
            displayTextView.text = currentText + buttonText
        }

        val button8: Button = binding.buttonEight
        button8.setOnClickListener {
            val currentText = displayTextView.text.toString()
            val buttonText = binding.buttonEight.text.toString()
            displayTextView.text = currentText + buttonText
        }

        val button9: Button = binding.buttonNine
        button9.setOnClickListener {
            val currentText = displayTextView.text.toString()
            val buttonText = binding.buttonNine.text.toString()
            displayTextView.text = currentText + buttonText
        }

        val buttonPlus: Button = binding.buttonPlus
        buttonPlus.setOnClickListener {
            val currentText = displayTextView.text.toString()
            val buttonText = binding.buttonPlus.text.toString()
            displayTextView.text = currentText + buttonText
        }

        val buttonMinus: Button = binding.buttonMinus
        buttonMinus.setOnClickListener {
            val currentText = displayTextView.text.toString()
            val buttonText = binding.buttonMinus.text.toString()
            displayTextView.text = currentText + buttonText
        }

        val buttonMultiply: Button = binding.buttonMultiply
        buttonMultiply.setOnClickListener {
            val currentText = displayTextView.text.toString()
            val buttonText = binding.buttonMultiply.text.toString()
            displayTextView.text = currentText + buttonText
        }
    }
}
